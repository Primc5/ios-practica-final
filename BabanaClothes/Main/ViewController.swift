//
//  ViewController.swift
//  BabanaClothes
//
//  Created by Gloria on 8/1/19.
//  Copyright © 2019 Team Excelencia. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController {
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var mujer: UIButton!
    @IBOutlet weak var hombre: UIButton!
    let searchController = UISearchController(searchResultsController: nil)
    internal var categories:[Categories] = []
    internal var clothes:[Clothes] = []

    
    init(){
        super.init(nibName: "ViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "compras")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        let dress1 = ClothesItem(clothesImg: "https://i.blogs.es/9d8570/tarta-tropezienne-dap/450_1000.jpg", price: "500.00 $", title: "Vestido Gucci", subtitle: "Vestido azul", descrption: "jhjgcvjgcjg", size: "Small")
        let dress = Categories(clothestipe: "Vestidos", clothesImage: "https://i.blogs.es/9d8570/tarta-tropezienne-dap/450_1000.jpg", description: "ugfiugiug", clothes: [])
        categories.append(dress)
        
    }

    private func registerCells() {
        let identifier = "CategoryCell"
        let cellNib = UINib(nibName: identifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: identifier)
    }
 

}
extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CategoryCell =
            tableView.dequeueReusableCell(withIdentifier:"CategoryCell", for:indexPath) as! CategoryCell
        let category = categories[indexPath.row]
        
        cell.categoryimg.sd_setImage(with: URL(string: category.clothesImage), placeholderImage: UIImage(named: ""), options: .cacheMemoryOnly, completed: nil)
        cell.category.text = category.clothestipe
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mycategories = categories[indexPath.row]
        let clothesVC = ClothesTipeViewController(clothes: mycategories.clothes)
        navigationController?.pushViewController(clothesVC, animated: true)
    }
}


