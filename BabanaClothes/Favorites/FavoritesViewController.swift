//
//  FavoritesViewController.swift
//  BabanaClothes
//
//  Created by Gloria on 8/1/19.
//  Copyright © 2019 Team Excelencia. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    
    init(){
        super.init(nibName: "FavoritesViewController", bundle: nil)
        self.tabBarItem.image = UIImage(named: "estrella")
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
