//
//  Categories.swift
//  BabanaClothes
//
//  Created by Gloria on 14/1/19.
//  Copyright © 2019 Team Excelencia. All rights reserved.
//

import Foundation
class Categories {
    var clothestipe: String!
    var clothesImage: String!
    var clothes:[Clothes] = []
    
    
    init(clothestipe: String, clothesImage: String, description: String, clothes:[Clothes]) {
        self.clothestipe = clothestipe
        self.clothesImage = clothesImage
        self.clothes = clothes
        
    }
}
