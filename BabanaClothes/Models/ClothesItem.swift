//
//  ClothesItem.swift
//  BabanaClothes
//
//  Created by Gloria on 14/1/19.
//  Copyright © 2019 Team Excelencia. All rights reserved.
//

import Foundation
class ClothesItem {
    var clothesImg: String!
    var price: String!
    var title: String!
    var subtitle: String!
    var descrption: String!
    var size: String!
    init(clothesImg: String, price: String, title: String, subtitle: String, descrption: String, size: String) {
        self.clothesImg = clothesImg
        self.price = price
        self.title = title
        self.subtitle = subtitle
        self.descrption = descrption
        self.size = size
    }
}
