//
//  Clothes.swift
//  BabanaClothes
//
//  Created by Gloria on 14/1/19.
//  Copyright © 2019 Team Excelencia. All rights reserved.
//

import Foundation
class Clothes {
    var clothesImg: String!
    var price: String!
    var title: String!
    var subtitle: String!
    var descrption: String!
    var clothesItem:[ClothesItem] = []
    init(clothesImg: String, price: String, title: String, subtitle: String, descrption: String, clothesItem:[ClothesItem]) {
        self.clothesImg = clothesImg
        self.price = price
        self.title = title
        self.subtitle = subtitle
        self.descrption = descrption
        self.clothesItem = clothesItem
        
    }
}
